const fs = require('fs');
const path = require('path');

function cardsDataForListId(listId, callback) {

    setTimeout(() => {
        const cardsFile = 'data/cards.json';
        const cardsFilePath = path.join(__dirname, cardsFile);
        const givenListId = listId;

        fs.readFile(cardsFilePath, (err, data) => {
            if(err){
                callback(err);
            }
            else{
                data = JSON.parse(data);
                if (Object.keys(data).includes(givenListId)){
                    listIdCardsData = [givenListId, data[givenListId]];
                }
                else{
                    listIdCardsData = [givenListId,[]];
                }

                callback(err, listIdCardsData);
            }
        })
    }, 2 * 1000);
}

module.exports = cardsDataForListId;