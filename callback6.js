const callback1 = require('./callback1.js');
const callback2 = require('./callback2.js');
const callback3 = require('./callback3.js');

function allListsAllCardsDataForThanosBoard() {

    setTimeout(() => {
        const thanosId = "mcu453ed";

        callback1(thanosId, (err, data) => {
            if (err) {
                console.error(err);
            }
            else {
                console.log("all data for thanos boards: ", data);
            }
        });

        callback2(thanosId, (err, data) => {
            if (err) {
                console.error('error');
                allListsForAllCards(err);
            }
            else {
                console.log("all lists for the thanos board : ", data[1]);
                allListsForAllCards(null, data);
            }
        })

        function allListsForAllCards(err, data) {
            if (err) {
                console.error("cannot retrieve mind cards data ", err);
            }
            else {
                for (let index = 0; index < data[1].length; index++) {
                    callback3(data[1][index]['id'], (err, data) => {
                        if (err) {
                            console.error(`cannot get the details of ${data[index]['name']} An error occurred`, err);
                        }
                        else {
                            console.log(`cards for ${data[0]} is: `, data[1]);
                        }
                    });
                    

                }
            }
        }
    }, 2 * 1000);
}

module.exports = allListsAllCardsDataForThanosBoard;