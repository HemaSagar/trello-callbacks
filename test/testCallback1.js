const getBoardIdData = require('../callback1.js');
const fs = require('fs');
const path = require('path');

const boardsFile = '../data/boards.json';
const boardsFilePath = path.join(__dirname,boardsFile);

getBoardIdData('mcu453ed',(err,data) => {
    if(err){
        console.log(`${err}`)
    }
    else{
        console.log(data);
    }
});