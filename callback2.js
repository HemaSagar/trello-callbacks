const fs = require('fs');
const path = require('path');

function listDataForBoardId(boardId, callback) {

    setTimeout(() => {
        const listsFile = 'data/lists.json';
        const listsFilePath = path.join(__dirname, listsFile);
        const givenBoardId = boardId;

        fs.readFile(listsFilePath, (err, data) => {
            if (err) {
                callback(err);
            }
            else {
                const boardIdListData = [boardId, JSON.parse(data)[givenBoardId]];
                callback(null, boardIdListData);
            }
        })
    }, 2 * 1000);
}


module.exports = listDataForBoardId;