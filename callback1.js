const fs = require('fs');
const path = require('path');



function getBoardIdData(boardId, callback) {
    setTimeout(() => {
        const boardsFile = 'data/boards.json';
        const boardsFilePath = path.join(__dirname, boardsFile);

        fs.readFile(boardsFilePath, (err, data) => {
            if (err) {
                callback(err);
            }   
            else {
                data = JSON.parse(data);

                const requiredIdData = data.filter((item) => {
                    return item.id === boardId;
                })
                callback(null,requiredIdData);

            }
        })
    }, 2 * 1000);
}



module.exports =  getBoardIdData;