const callback1 = require('./callback1.js');
const callback2 = require('./callback2.js');
const callback3 = require('./callback3.js');

function mindAndSpaceListForThanosBoard() {

    setTimeout(() => {
        const thanosId = "mcu453ed";

        callback1(thanosId, (err, data) => {
            if (err) {
                console.error(err);
            }
            else {
                console.log("all data for thanos boards: ", data);
            }
        });

        callback2(thanosId, (err, data) => {
            if (err) {
                console.error('error');
                mindData(err);
            }
            else {
                console.log("all lists for the thanos board : ", data[1]);
                mindAndSpaceData(null, data);
            }
        })

        function mindAndSpaceData(err, data) {
            if (err) {
                console.error("cannot retrieve mind cards data ", err);
            }
            else {
                const mindSpaceData = data[1].filter((card) => {
                    return (card['name'] === 'Mind' || card['name'] === 'Space');
                })

                let errors = 0;
                let executed = 0;
                for (let index = 0; index < mindSpaceData.length; index++) {
                    callback3(mindSpaceData[index].id, (err, data) => {
                        if (err) {
                            errors += 1;
                            console.error(`cannot get the details of ${mindSpaceData[index]['name']} An error occurred`, err);
                        }
                        else {
                            executed += 1;
                            console.log(`cards for ${mindSpaceData[index]['name']} is: `, data[1]);

                        }
                        if((errors+executed) == mindSpaceData.length){
                            console.log('all tasks executed succesfully');
                        }
                    });

                }
            }
        }
    }, 2 * 1000);
}

module.exports = mindAndSpaceListForThanosBoard;