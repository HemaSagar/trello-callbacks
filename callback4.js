const callback1 = require('./callback1.js');
const callback2 = require('./callback2.js');
const callback3 = require('./callback3.js');

function mindListForThanosBoard() {

    setTimeout(() => {
        const thanosId = "mcu453ed";
        let mindListId;

        callback1(thanosId, (err, data)=>{
            if(err){
                console.error(err);
            }
            else{
                console.log("all data for thanos boards: ", data);
            }
        });

        callback2(thanosId, (err,data)=>{
            if(err){
                console.error('error');
                mindData(err);
            }
            else{
                console.log("all lists for the thanos board : ",data[1]);
                mindData(null, data);
            }
        })

        function mindData(err,data){
            if(err){
                console.error("cannot retrieve mind cards data ",err);
            }
            else{
                const mindData = data[1].filter((card)=>{
                    return card['name'] === 'Mind';
                })
                mindListId = mindData[0].id;

                callback3(mindListId,(err,data) => {
                    if(err){
                        console.error('cannot get the details of mind. An error occurred', err);
                    }
                    else{
                        console.log("cards for the mind list: ",data[1]);
                        console.log("all tasks executed succesfully");
                    }
                })
            }
        }


    }, 2 * 1000);
}

module.exports = mindListForThanosBoard;